package com.foldem.eval;

import gnu.trove.map.hash.TLongIntHashMap;

import com.foldem.Card;

public class CachingFSMHandRankEvaluator implements HandRankEvaluator {
    
    private TLongIntHashMap cache = new TLongIntHashMap(); 
    private FSMHandRankEvaluator eval = new FSMHandRankEvaluator();

    @Override
    public int evaluate(Card... cards) throws EvaluationFailedException {
        return eval.evaluate(cards);
    }

    @Override
    public int evaluate(int... cardOrdinals) throws EvaluationFailedException {
        return eval.evaluate(cardOrdinals);
    }

    @Override
    public int evaluate(long cards) throws EvaluationFailedException {
        if(cache.containsKey(cards)) {
            return cache.get(cards);
        }
        int retVal = eval.evaluate(cards);
        cache.put(cards, retVal);
        return retVal;
    }
    
    
}
