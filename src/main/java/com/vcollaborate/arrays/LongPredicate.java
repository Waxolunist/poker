package com.vcollaborate.arrays;

public interface LongPredicate {
    boolean apply(long input);
}
