package com.vcollaborate.arrays;

import java.util.Arrays;

public final class ArrayUtils {

    public static final long[] filter(final long[] unfiltered, final LongPredicate predicate) {
        long[] filtered = new long[unfiltered.length];
        int j = 0;
        for(int i = 0; j < filtered.length; i++) {
            if(predicate.apply(unfiltered[i])) {
                filtered[j++] = unfiltered[i];
            }
        }
        
        return Arrays.copyOf(filtered, j);
    }
    
    public static final long[] filter(final long[] unfiltered, final int filteredLength, final LongPredicate predicate) {
        long[] filtered = new long[filteredLength];
        int j = 0;
        for(int i = 0; j < filteredLength; i++) {
            if(predicate.apply(unfiltered[i])) {
                filtered[j++] = unfiltered[i];
            }
        }
        
        return filtered;
    }    

}
