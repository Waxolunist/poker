package com.vcollaborate.poker.biteval;

public interface BitEvaluator {
    boolean isOfType(long value);
}
