package com.vcollaborate.poker;

import java.util.List;
import java.util.NavigableSet;

import lombok.extern.slf4j.Slf4j;

import com.google.common.collect.ImmutableSortedSet;
import com.vcollaborate.arrays.LongPredicate;
import com.vcollaborate.bitwise.BinaryUtils;

@Slf4j
public enum ProbabilitiesCalculator {
    INSTANCE;

    public double getSevenCardsProbability(final FiveCards cards) {
        log.debug("FiveCards: {}", cards);
        long[] filtered = com.vcollaborate.arrays.ArrayUtils.filter(CardsFactory.SEVEN.getAllPermutations(), new LongPredicate() {

            @Override
            public boolean apply(final long input) {
                return BinaryUtils.getHammingDistance(cards.getBinaryValue(), input) == 2;
            }
        });
        
        log.debug("Filterd: {}", filtered.length);
        
//        List<SevenCards> fclist = CombinationsCalculator.getCombinations(CardsFactory.SEVEN, filtered, 0, -1);
//        log.debug("FCList: {}", fclist.size());
//        NavigableSet<SevenCards> sorted = new ImmutableSortedSet.Builder<SevenCards>(CardsComparator.INSTANCE).addAll(fclist).build();
//        log.debug("Sorted: {}", sorted.size());
//        int sizeTail = sorted.tailSet(cards, true).size(); //less
//        log.debug("SizeTail: {}", sizeTail);
//        return sizeTail / fclist.size();
        return 0;
    }
}
