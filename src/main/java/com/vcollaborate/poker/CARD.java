package com.vcollaborate.poker;

import com.vcollaborate.bitwise.BinaryStringUtils;

public enum CARD {
        ACE_SPADE,
        ACE_HEART,
        ACE_DIAMOND,
        ACE_CLUB,

        KING_SPADE,
        KING_HEART,
        KING_DIAMOND,
        KING_CLUB,
        
        QUEEN_SPADE,
        QUEEN_HEART,
        QUEEN_DIAMOND,
        QUEEN_CLUB,
        
        JACK_SPADE,
        JACK_HEART,
        JACK_DIAMOND,
        JACK_CLUB,
        
        TEN_SPADE,
        TEN_HEART,
        TEN_DIAMOND,
        TEN_CLUB,
        
        NINE_SPADE,
        NINE_HEART,
        NINE_DIAMOND,
        NINE_CLUB,
        
        EIGHT_SPADE,
        EIGHT_HEART,
        EIGHT_DIAMOND,
        EIGHT_CLUB,
        
        SEVEN_SPADE,
        SEVEN_HEART,
        SEVEN_DIAMOND,
        SEVEN_CLUB,

        SIX_SPADE,
        SIX_HEART,
        SIX_DIAMOND,
        SIX_CLUB,

        FIVE_SPADE,
        FIVE_HEART,
        FIVE_DIAMOND,
        FIVE_CLUB,

        FOUR_SPADE,
        FOUR_HEART,
        FOUR_DIAMOND,
        FOUR_CLUB,

        THREE_SPADE,
        THREE_HEART,
        THREE_DIAMOND,
        THREE_CLUB,
        
        TWO_SPADE,
        TWO_HEART,
        TWO_DIAMOND,
        TWO_CLUB;
        
        public static final long CARDS_IN_DECK = 52L;
        
        public String binaryString() {
            return Long.toBinaryString(binaryValue());
        }
        
        public Long binaryValue() {
            return 1L << CARDS_IN_DECK - 1 - this.ordinal();
        }
        
        public String prettyBinaryString() {
            String binary = BinaryStringUtils.zeroPadString(binaryString(), (int) CARDS_IN_DECK); 
            return BinaryStringUtils.prettyPrint(binary);
        }
        
        public static CARD fromBinaryString(final String binaryString) {
            return CARD.values()[BinaryStringUtils.zeroPadString(BinaryStringUtils.fromPrettyString(binaryString), (int) CARDS_IN_DECK).indexOf('1')];
        }
}
