package com.vcollaborate.poker;

import com.vcollaborate.poker.eval.FiveEval;


public class FiveCards extends AbstractCards {

    public static final int NROFCARDS = 5; 
    
    FiveCards() {
        super(NROFCARDS, FiveEval.INSTANCE);
    }
}
