package com.vcollaborate.poker;

import com.vcollaborate.poker.eval.SevenEval;


public class SevenCards extends AbstractCards {

    public static final int NROFCARDS = 7;
    
    SevenCards() {
        super(NROFCARDS, SevenEval.INSTANCE);
    }
}
