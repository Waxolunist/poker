package com.vcollaborate.poker.gui;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class GameMainFrame extends JFrame {

    private static final long serialVersionUID = 3492970723098951476L;

    private JTabbedPane tabbedPane;

    public GameMainFrame() {
        init();
    }

    private void init() {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | UnsupportedLookAndFeelException | InstantiationException
                | IllegalAccessException e) {
            log.error("Error.", e);
        }

        Toolkit tk = Toolkit.getDefaultToolkit();
        Dimension screenSize = tk.getScreenSize();
        int screenHeight = screenSize.height;
        int screenWidth = screenSize.width;
        setLocation(screenWidth / 4, screenHeight / 4);

        this.tabbedPane = new JTabbedPane();

        tabbedPane.addTab("Probability Brute Force", new ProbabilityCalculator());

        this.add(this.tabbedPane);

        this.pack();
    }
}
