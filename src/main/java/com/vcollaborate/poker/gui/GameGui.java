package com.vcollaborate.poker.gui;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class GameGui {
    public GameGui() {
        GameMainFrame mainframe = new GameMainFrame();
        mainframe.setTitle("Poker 0.1");
        mainframe.addWindowListener(new WindowAdapter() {
          public void windowClosing(WindowEvent e) {
            System.exit(0);
          }
        });

        mainframe.setVisible(true);
      }

      /**
       * @param args
       */
      public static void main(String[] args) {
        new GameGui();
      }

}
