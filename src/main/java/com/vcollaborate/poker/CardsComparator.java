package com.vcollaborate.poker;

import java.util.Comparator;

public enum CardsComparator implements Comparator<AbstractCards> {
    INSTANCE;
    
    @Override
    public int compare(AbstractCards o1, AbstractCards o2) {
        return o1.getRank() - o2.getRank();
    }
    
}
