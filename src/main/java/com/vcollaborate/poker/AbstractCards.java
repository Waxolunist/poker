package com.vcollaborate.poker;

import static com.vcollaborate.poker.Constants.DECK_SIZE;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import com.google.common.base.Preconditions;
import com.vcollaborate.bitwise.BinaryUtils;
import com.vcollaborate.poker.eval.Eval;

@ToString
@EqualsAndHashCode(exclude={"eval","cards","rank","nrOfCards"})
public abstract class AbstractCards implements Comparable<AbstractCards>, Cloneable {
    
    private CARD[] cards;

    @Getter
    private int nrOfCards;

    @Getter
    private Eval eval;
    
    @Getter
    private int rank;
    
    @Getter
    private long binaryValue;

    AbstractCards(int nrOfCards, Eval eval) {
        this.nrOfCards = nrOfCards;
        this.eval = eval;
    }
    
    void setCards(CARD...cards) {
        Preconditions.checkArgument(cards.length == nrOfCards);
        
        this.cards = cards;

        for(CARD c : cards) {
            Preconditions.checkNotNull(c);
        }

        this.binaryValue = binaryValue();
        
        Preconditions.checkArgument(isValid());
        
        this.rank = rank();
    }

    private Long binaryValue() {
        long retVal = 0;
        for(CARD c : cards) {
            retVal |= c.binaryValue();
        }
        return retVal;
    }

    public String binaryString() {
        return Long.toBinaryString(binaryValue());
    }

    private boolean isValid() {
        return Long.bitCount(binaryValue) == this.nrOfCards;
    }

    protected int rank() {
        return eval.getRankOf(CardsFactory.SEVEN.toIntegerArray(cards));
    }

    public int compareTo(AbstractCards o) {
        return getRank() - o.getRank();
    }
    
    public long getMinVal() {
        return BinaryUtils.allOnes(nrOfCards);
    }
    
    public long getMaxVal() {
        return getMinVal() << (DECK_SIZE - nrOfCards);
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        try {
            AbstractCards ac = this.getClass().newInstance();
            return ac;
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }
}

