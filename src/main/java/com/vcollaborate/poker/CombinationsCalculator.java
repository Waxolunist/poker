package com.vcollaborate.poker;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;

public class CombinationsCalculator<T extends AbstractCards> extends RecursiveTask<List<T>> {
    
    private static final long serialVersionUID = -4159189223120464010L;

    static final int THRESHOLD = 100;

    int lowerBoundary;
    int upperBoundary;
    CardsFactory cardsfactory;
    long[] permutations;
    
    public static final <T extends AbstractCards> List<T> getCombinations(final CardsFactory cardsfactory, final long[] permutations, final int lower, final int upper) {
        ForkJoinPool fjp = new ForkJoinPool();
        if(upper < 0) {
            return fjp.invoke(new CombinationsCalculator<T>(cardsfactory, permutations, lower, permutations.length));
        } else {
            return fjp.invoke(new CombinationsCalculator<T>(cardsfactory, permutations, lower, upper));
        }
    }
   
    
    public CombinationsCalculator(final CardsFactory cardsfactory, final long[] permutations, final int lowerBoundary, final int upperBoundary) {
        super();
        this.lowerBoundary = lowerBoundary;
        this.upperBoundary = upperBoundary;
        this.cardsfactory = cardsfactory;
        this.permutations = permutations;
    }



    @Override
    protected List<T> compute() {
        if (upperBoundary - lowerBoundary < THRESHOLD) {
            //log.info("compute - index: {} - {}", lowerBoundary, upperBoundary);
            return createCards();
        } else {
            int middle = lowerBoundary + (upperBoundary - lowerBoundary) / 2;
            CombinationsCalculator<T> c1 = new CombinationsCalculator<>(cardsfactory, permutations, lowerBoundary, middle);
            CombinationsCalculator<T> c2 = new CombinationsCalculator<>(cardsfactory, permutations, middle, upperBoundary);
            c1.fork();
            List<T> cards = new ArrayList<>(upperBoundary - lowerBoundary + 1); 
            cards.addAll(c2.compute());
            cards.addAll(c1.join());
            return cards;
        }
    }

    @SuppressWarnings("unchecked")
    private List<T> createCards() {
        List<T> cards = new ArrayList<>(upperBoundary - lowerBoundary + 1);
        for(int i = lowerBoundary; i < upperBoundary; i++) {
            cards.add((T) cardsfactory.getNewInstance(permutations[i]));
        }
        return cards;
    }

}
