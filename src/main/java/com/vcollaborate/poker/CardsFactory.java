package com.vcollaborate.poker;

import static com.vcollaborate.poker.Constants.DECK_SIZE;

import java.util.Arrays;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import com.google.common.base.Preconditions;
import com.vcollaborate.bitwise.BinaryUtils;
import com.vcollaborate.poker.eval.Eval;

@Slf4j
public enum CardsFactory {
    SEVEN(SevenCards.class),
    FIVE(FiveCards.class);
    
    @Getter
    int nrOfCards;
    
    Eval eval;
    
    AbstractCards prototype;
    
    @Getter
    long[] allPermutations;
    
    <T extends AbstractCards> CardsFactory(Class<T> clazz) {
        try {
            this.prototype = clazz.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        Preconditions.checkArgument(prototype != null);
        this.nrOfCards = prototype.getNrOfCards();
        this.eval = prototype.getEval();
        this.allPermutations = BinaryUtils.getAllPermutations(DECK_SIZE, this.nrOfCards);
    }
    
    @SuppressWarnings("unchecked")
    private <T extends AbstractCards> T getClone() {
        try {
            return (T) prototype.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    public <T extends AbstractCards> T getNewInstance(CARD...cards) {
        Preconditions.checkArgument(cards.length == nrOfCards);
        T ac = getClone();
        ac.setCards(cards);
        return ac;
    }
    
    public <T extends AbstractCards> T getNewInstance(long value) {
        return getNewInstance(fromBinary(value));
    }
    
    public CARD[] fromBinary(long value) {
        int[] bits = BinaryUtils.getBitsSet(value);
        CARD[] cards = new CARD[Long.bitCount(value)];
        for (int i = 0; i < cards.length; i++) {
            cards[i] = CARD.values()[DECK_SIZE - 1 -bits[i]];
        }
        return cards;
    }
    
    public int[] toIntegerArray(CARD[] cards) {
        int[] retVal = new int[cards.length];
        for(int i = 0; i < cards.length; i++) {
            retVal[i] = cards[i].ordinal();
        }
        return retVal;
    }
}
