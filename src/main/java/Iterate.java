import java.util.Arrays;
import java.util.Collection;

import org.apache.commons.lang.ArrayUtils;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.ImmutableList;
import com.vcollaborate.arrays.LongPredicate;
import com.vcollaborate.bitwise.BinaryUtils;
import com.vcollaborate.poker.SevenCards;

public class Iterate {

    /**
     * @param args
     */
    public static void main(String[] args) {
        long start = System.currentTimeMillis();    
        long[] allPermutations = BinaryUtils.getAllPermutations(52, 7);
//        for(int i = 0; i < 100; i++) {
//            System.out.println(allPermutations[i] + " " + Long.toBinaryString(allPermutations[i]));
//        }
        
        System.out.println(allPermutations.length);
        
        final long bitmask = (1 << 5) - 1;
        System.out.println(bitmask + " " + Long.toBinaryString(bitmask));
        
        long[] filtered = com.vcollaborate.arrays.ArrayUtils.filter(allPermutations, new LongPredicate() {
            
            @Override
            public boolean apply(long input) {
                return BinaryUtils.getHammingDistance(bitmask, input) == 2;
            }
        });
        
        System.out.println(filtered.length);
        long stop = System.currentTimeMillis();
        System.out.println((stop - start) / 1000.0 + " seconds");
    }

}
