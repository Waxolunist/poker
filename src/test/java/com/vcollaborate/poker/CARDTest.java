package com.vcollaborate.poker;

import static com.vcollaborate.poker.CARD.ACE_CLUB;
import static com.vcollaborate.poker.CARD.ACE_DIAMOND;
import static com.vcollaborate.poker.CARD.ACE_HEART;
import static com.vcollaborate.poker.CARD.ACE_SPADE;
import static com.vcollaborate.poker.CARD.JACK_CLUB;
import static com.vcollaborate.poker.CARD.KING_DIAMOND;
import static com.vcollaborate.poker.CARD.KING_HEART;
import static com.vcollaborate.poker.CARD.KING_SPADE;
import static com.vcollaborate.poker.CARD.TWO_CLUB;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import java.util.List;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import com.vcollaborate.arrays.LongPredicate;
import com.vcollaborate.bitwise.BinaryUtils;

@Slf4j
public class CARDTest {

    @Test
    public void testIntegerRepresentation() {
         assertEquals(0, ACE_SPADE.ordinal());
    }
    
    @Test
    public void testBinaryRepresenation() {
        for(CARD c : CARD.values()) {
            log.info(StringUtils.EMPTY);
            log.info("{}", c);
            log.info(c.binaryString());
            log.info(c.prettyBinaryString());
            log.info("{}", CARD.fromBinaryString(c.prettyBinaryString()));
            log.info("{}", CARD.fromBinaryString(c.binaryString()));
            
            assertEquals(c, CARD.fromBinaryString(c.prettyBinaryString()));
            assertEquals(c, CARD.fromBinaryString(c.binaryString()));
        }
    }
    
    @Test
    public void testBinaryRepresenation7Cards1() {
        SevenCards sc1 = CardsFactory.SEVEN.getNewInstance(ACE_SPADE, ACE_CLUB, ACE_DIAMOND, ACE_HEART, KING_SPADE, KING_HEART, KING_DIAMOND);
        SevenCards sc2 = CardsFactory.SEVEN.getNewInstance(ACE_SPADE, ACE_DIAMOND, ACE_HEART, KING_SPADE, KING_HEART, KING_DIAMOND, JACK_CLUB);
        assertTrue(sc1.compareTo(sc2) > 0);
        
        log.info("{} - {}", sc1.getBinaryValue(), sc1.binaryString());
        log.info("{} - {}", sc2.getBinaryValue(), sc2.binaryString());
        
        assertEquals(sc1, CardsFactory.SEVEN.getNewInstance(sc1.getBinaryValue()));
        assertEquals(sc2, CardsFactory.SEVEN.getNewInstance(sc2.getBinaryValue()));
    }
    
    @Test
    public void testBinaryRepresenation7Cards2() {
        SevenCards sc1 = CardsFactory.SEVEN.getNewInstance(ACE_SPADE, ACE_DIAMOND, ACE_HEART, KING_SPADE, KING_HEART, KING_DIAMOND, TWO_CLUB);
        SevenCards sc2 = CardsFactory.SEVEN.getNewInstance(ACE_SPADE, ACE_DIAMOND, KING_SPADE, KING_HEART, KING_DIAMOND, JACK_CLUB, ACE_HEART);
        assertTrue(sc1.compareTo(sc2) == 0);
        
        log.info("{}", sc1.binaryString());
        log.info("{}", sc2.binaryString());
        
        assertEquals(sc1, CardsFactory.SEVEN.getNewInstance(sc1.getBinaryValue()));
        assertEquals(sc2, CardsFactory.SEVEN.getNewInstance(sc2.getBinaryValue()));
    }

    @Test
    public void testFromBinary() {
        SevenCards sc = CardsFactory.SEVEN.getNewInstance(127);
        log.info("{}", sc);
        assertEquals(127, sc.getBinaryValue());
    }
    
    @Test
    public void testBinaryRepresenation5Cards1() {
        FiveCards fc1 = CardsFactory.FIVE.getNewInstance(ACE_SPADE, ACE_CLUB, ACE_DIAMOND, ACE_HEART, KING_SPADE);
        FiveCards fc2 = CardsFactory.FIVE.getNewInstance(ACE_SPADE, ACE_DIAMOND, ACE_HEART, KING_SPADE, KING_HEART);
        assertTrue(fc1.compareTo(fc2) > 0);
    }
    
    @Test
    public void testCombinationsCalculator() {
        int upper = 2000;
        List<SevenCards> sclist = CombinationsCalculator.getCombinations(CardsFactory.SEVEN, CardsFactory.SEVEN.getAllPermutations(), 0, upper);
        
        log.info("Calculated combinations: {}", sclist.size());
        
        assertEquals(upper, sclist.size());
    }
    
    @Test
    public void testProbabilitesCalculator() {
        FiveCards fc1 = CardsFactory.FIVE.getNewInstance(ACE_SPADE, ACE_CLUB, ACE_DIAMOND, ACE_HEART, KING_SPADE);
        double probability = ProbabilitiesCalculator.INSTANCE.getSevenCardsProbability(fc1);
        log.info("Probability: {}", probability);
        assertEquals(1.0, probability, 0);
    }
    
    @Test
    public void testFilter() {
        final FiveCards cards = CardsFactory.FIVE.getNewInstance(ACE_SPADE, ACE_CLUB, ACE_DIAMOND, ACE_HEART, KING_SPADE);
        log.debug("FiveCards: {}", cards);
        long[] filtered = com.vcollaborate.arrays.ArrayUtils.filter(CardsFactory.SEVEN.getAllPermutations(), new LongPredicate() {

            @Override
            public boolean apply(final long input) {
                return BinaryUtils.getHammingDistance(cards.getBinaryValue(), input) == 2;
            }
        });
        
        log.debug("Filterd: {}", filtered.length);
        
        List<SevenCards> fclist = CombinationsCalculator.getCombinations(CardsFactory.SEVEN, filtered, 0, -1);
        log.debug("FCList: {}", fclist.size());
        
        assertEquals(1081, fclist.size());
        assertFalse(fclist.get(0).equals(fclist.get(1)));
        assertFalse(fclist.get(20).equals(fclist.get(21)));
    }
}
