package com.vcollaborate.poker.eval;

import junit.framework.TestCase;

import org.junit.Test;

import com.vcollaborate.poker.eval.HandEval;

public class HandEvalTest {

    @Test
    public void testTimeRank() {
        HandEval eval = HandEval.INSTANCE;
        int[] holeCards = new int[] {0, 11, 37, 38};
        System.out.println(eval.computePreFlopEquityForSpecificHoleCards(holeCards, 2));
        eval.timeRankMethod();
    }
    
}
