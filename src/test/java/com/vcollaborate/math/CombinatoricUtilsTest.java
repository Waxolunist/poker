package com.vcollaborate.math;

import static org.junit.Assert.assertEquals;
import lombok.extern.slf4j.Slf4j;

import org.junit.Test;

@Slf4j
public class CombinatoricUtilsTest {

    @Test
    public void testCombinations() {
        
        long combinations7Cards = CombinatoricUtils.combinations(52, 7);
        log.debug("Possible 7 Card combinations (52,7): {}", combinations7Cards);
        assertEquals(133_784_560L, combinations7Cards);
        
        long combinations5Cards = CombinatoricUtils.combinations(52, 5);
        log.debug("Possible 5 Card combinations (52,5): {}", combinations5Cards);
        assertEquals(2_598_960L, combinations5Cards);
        
        long combinationsOpponentsCards = CombinatoricUtils.combinations(52, 5);
        log.debug("Possible 7 Card combinations - opponent (50,7): {}", combinationsOpponentsCards);
        assertEquals(2_598_960L, combinationsOpponentsCards);
        
        long combinationsAfterRiverCards = CombinatoricUtils.combinations(47, 2);
        log.debug("Possible 7 Card combinations - after river (47,2): {}", combinationsAfterRiverCards);
        assertEquals(1081L, combinationsAfterRiverCards);
        
        long combinationsAfterRiverOpponentCards = CombinatoricUtils.combinations(47, 4);
        log.debug("Possible 7 Card combinations - after river opponent (47,4): {}", combinationsAfterRiverOpponentCards);
        assertEquals(178365L, combinationsAfterRiverOpponentCards);
        
    }

}
