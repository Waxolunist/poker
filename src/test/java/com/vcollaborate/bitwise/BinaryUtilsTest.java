package com.vcollaborate.bitwise;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import lombok.extern.slf4j.Slf4j;

import org.junit.Test;

import com.vcollaborate.arrays.LongPredicate;
import com.vcollaborate.math.CombinatoricUtils;

@Slf4j
public class BinaryUtilsTest {

    @Test
    public void testGetAllPermutations() {
        long[] allPermutations1 = BinaryUtils.getAllPermutations(10, 1);
        assertArrayEquals(new long[]{1L, 2L, 4L, 8L, 16L, 32L, 64L, 128L, 256L, 512L}, allPermutations1);
        
        long[] allPermutations2 = BinaryUtils.getAllPermutations(8, 2);
        assertEquals(allPermutations2[0], 3);
        assertEquals(allPermutations2[allPermutations2.length - 1], 0x3 << 6);
    }

    @Test
    public void testGetAllPermutationsLoad() {
        long[] times = new long[10];
        for(int i = 0; i < times.length; i++) {
            System.gc();
            long start = System.currentTimeMillis();
            BinaryUtils.getAllPermutations(52, 7);
            times[i] = System.currentTimeMillis() - start;
        }
        
        long avg = 0;
        for(int i = 0; i < times.length; i++) {
            avg += times[i];
        }
        Arrays.sort(times);
        log.info("Avg: {} ms, Min: {} ms, Max: {} ms", avg / times.length, times[0], times[times.length - 1]);
    }
    
    @Test
    public void testGetHammingDistance() {
        long val1 = BinaryUtils.allOnes(7);
        long val2 = BinaryUtils.allOnes(2);
        
        assertEquals(5, BinaryUtils.getHammingDistance(val1, val2));
    }
 
    @Test
    public void testIsBitsSet() {
        assertTrue(BinaryUtils.isBitSet(1, 0));
        assertTrue(BinaryUtils.isBitSet(3, 0));
        assertTrue(BinaryUtils.isBitSet(3, 1));
        assertFalse(BinaryUtils.isBitSet(3, 3));
        assertTrue(BinaryUtils.isBitSet(128, 7));
    }
    
    @Test
    public void testGetBitsSet() {
        int[] ba1 = BinaryUtils.getBitsSet(0xFF);
        log.info("{}", Arrays.toString(ba1));
        assertArrayEquals(new int[]{0,1,2,3,4,5,6,7}, ba1);
        
        int[] ba2 = BinaryUtils.getBitsSet(0xF1);
        log.info("{}", Arrays.toString(ba2));
        assertArrayEquals(new int[]{0,4,5,6,7}, ba2);
    }
    
    @Test
    public void testBitFilter() {
        final long bitmask = BinaryUtils.allOnes(5);
        final long[] unfiltered = BinaryUtils.getAllPermutations(52, 7);
        long[] filtered = com.vcollaborate.arrays.ArrayUtils.filter(unfiltered, new LongPredicate() {
            @Override
            public boolean apply(final long input) {
                return BinaryUtils.getHammingDistance(bitmask, input) == 2;
            }
        });
        assertEquals(1081, filtered.length);
    }
    

    @Test
    public void testBitFilterLoad() {
        long[] times = new long[10];
        
        final long bitmask = BinaryUtils.allOnes(5);
        final long[] unfiltered = BinaryUtils.getAllPermutations(52, 7);
        final int newLength = CombinatoricUtils.combinations(47, 2);
        
        for(int i = 0; i < times.length; i++) {
            System.gc();
            long start = System.currentTimeMillis();
            
            com.vcollaborate.arrays.ArrayUtils.filter(unfiltered, newLength, new LongPredicate() {
                @Override
                public boolean apply(final long input) {
                    return BinaryUtils.getHammingDistance(bitmask, input) == 2;
                }
            });
        
            times[i] = System.currentTimeMillis() - start;
        }
        
        long avg = 0;
        for(int i = 0; i < times.length; i++) {
            avg += times[i];
        }
        Arrays.sort(times);
        log.info("Avg: {} ms, Min: {} ms, Max: {} ms", avg / times.length, times[0], times[times.length - 1]);
    }
    
    @Test
    public void testBitFilterLoad2() {
        long[] times = new long[10];
        
        //river
        final long bitmask1 = BinaryUtils.allOnes(3);
        //your hand
        final long bitmask2 = BinaryUtils.allOnes(2) << 3L;
        
        log.info("Bitmasks: {}, {}", Long.toBinaryString(bitmask1), Long.toBinaryString(bitmask2));
        
        final long[] unfiltered = BinaryUtils.getAllPermutations(52, 7);
        
        for(int i = 0; i < 10; i++) {
            System.gc();
            long start = System.currentTimeMillis();
            
            long[] filtered1 = com.vcollaborate.arrays.ArrayUtils.filter(unfiltered, new LongPredicate() {
                @Override
                public boolean apply(final long input) {
                    return BinaryUtils.getHammingDistance(bitmask1, input) == 4 &&
                           BinaryUtils.getHammingDistance(bitmask2, input) == 9;
                }
            });
            times[i] = System.currentTimeMillis() - start;
            log.info("Filtered 1: {}", filtered1.length);
        }
        
        long avg = 0;
        for(int i = 0; i < times.length; i++) {
            avg += times[i];
        }
        Arrays.sort(times);
        log.info("Avg: {} ms, Min: {} ms, Max: {} ms", avg / times.length, times[0], times[times.length - 1]);
    }
    
    @Test
    public void calc2RemainingCards() {
        long hand = 0b1100000000000000000000000000000000000000000000000000L;
        long river = 0b11100000000000000000000000000000000000000000000000L;
        long together = hand | river;
        
        log.info("Togehter: {}", Long.toBinaryString(together));
        
        long flopturn = 0b11;
        
        long complete = together | flopturn;
        log.info("Complete: {}", Long.toBinaryString(complete));
        
        assertEquals(7, Long.bitCount(complete));
        
        long shared = complete & (river | flopturn);
        log.info("Shared: {}", Long.toBinaryString(shared));
        assertEquals(5, Long.bitCount(shared));
    }
}
